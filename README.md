# Introduction to Git & Bitbucket Workshop

This repository contains the files needed for the LNCD workshop introducing Git and Bitbucket.

## Contributors

The following people have contributed to this workshop.

* Robert Bannister
* [Alex Bilbie](mailto:abilbie@lincoln.ac.uk)
* [Thomas Cucksey](mailto:09186675@students.lincoln.ac.uk)
* [Nick Jackson](mailto:nijackson@lincoln.ac.uk)
* [Harry Newton](mailto:hnewton@lincoln.ac.uk)
* [William Sawyer](mailto:11239411@students.lincoln.ac.uk)
* Kieran Wagg
* Sam Wilson